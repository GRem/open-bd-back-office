import Vue from 'vue'
import Router from 'vue-router'

import Books from './views/Books'
import Login from './views/Login'
import News from './views/News'
import Users from './views/Users'
import Tasks from './views/Tasks'
import Services from './views/Services'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/news',
      name: 'news',
      componenet: News
    },
    {
      path: '/books',
      name: 'books',
      componenet: Books
    },
    {
      path: '/users',
      name: 'users',
      componenet: Users
    },
    {
      path: '/tasks',
      name: 'tasks',
      componenet: Tasks
    },
    {
      path: '/services',
      name: 'services',
      component: Services
    }
  ]
})
