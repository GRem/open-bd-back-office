import Vue from 'vue'
import Vuex from 'vuex'
import storage from './storage'
import { abilityPlugin, ability as appAbility } from './ability'
import notifications from './notifications'
import articles from './articles'
import services from './resources/services'
import http from '../services/http'

Vue.use(Vuex)

export const ability = appAbility

export const store = new Vuex.Store({
  plugins: [
    storage({
      storedKeys: ['token'],
      destroyOn: ['destroySession']
    }),
    abilityPlugin
  ],

  modules: {
    notifications,
    articles,
    services
  },

  state: {
    token: ''
  },

  getters: {
    isLoggedIn(state) {
      return !!state.token
    }
  },

  mutations: {
    createSession(state, session) {
      state.token = session.token_type+" "+session.access_token
      http.token = state.token
    },

    destroySession(state) {
      state.token = ''
    }
  },

  actions: {
    login({ commit }, details) {
      return http('/sign/token', { method: 'POST', body: JSON.stringify(details) })
        .then(response => commit('createSession', response.body))
    },

    logout({ commit }) {
      commit('destroySession')
    }
  }
})

http.token = store.state.token
http.onError = response => {
  if (response.status === 403) {
    store.dispatch('notifications/error', response.body.message)
    return true
  }
}
