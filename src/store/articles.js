import http from '../services/http'

export default {
  namespaced: true,

  actions: {
    find() {
      return http('/books')
        .then(response => response.body.items)
    },

    findById(_, id) {
      return http(`/books/${id}`)
        .then(response => response.body.item)
    },

    destroy(_, book) {
      return http(`/books/${book.id}`, { method: 'DELETE' })
        .then(response => response.body.item)
    },

    save(_, book) {
      const request = book.id
        ? http(`/books/${book.id}`, { method: 'PUT', body: JSON.stringify(book) })
        : http('/books', { method: 'POST', body: JSON.stringify(book) })

      return request.then(response => response.body.item)
    }
  }
}
