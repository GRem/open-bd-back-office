import http from '../../services/http'

export default {
  namespaced: true,

  actions: {
    findAll() {
      return http('/services')
        .then(response => response.body.services)
    },
    create(_, data) {
      let body = {
        service: {
          name: data.name,
          description: data.description,
          url: data.url
        }
      }
      return http('/services', {
        method: 'POST', body: JSON.stringify(body)
      }).then(response => response.body.service)
    },
    update(_, data) {
      let body = {
        service: {
          name: data.name,
          description: data.description,
          url: data.url
        }
      }
      return http(`/services/${data.id}`, {
        method: 'PUT', body: JSON.stringify(body)
      }).then(response => response.body.service)
    },
    destroy(_, data) {
      return http(`/services/${data.id}`, {
        method: 'DELETE'
      })
    }
  }
}
